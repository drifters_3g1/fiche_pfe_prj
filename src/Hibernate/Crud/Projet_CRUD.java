/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.Etudiant;
import Hibernate.entite.Etudiant_Projet;
import Hibernate.entite.Projet;
import Hibernate.entite.SPE;
import cnx_config.cxn_statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Mehdi Aoussar
 */
public class Projet_CRUD {
    
    public static ArrayList<Projet> projets = new ArrayList<>();
    
     public static void insert(Projet p) {

        try {
            Hibernate_Config.Hibernat_Session().save(p);
            Hibernate_Config.Hibernat_Transaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    public static void Delete(Projet p) {

          try {
           Projet ds = (Projet) p;
        String sql = "delete from Projet where id = "+ds.getId();
                
        Connection cnt = cxn_statement.getConnection();
        Statement stm = cxn_statement.getStatement(cnt);
        
        stm.executeUpdate(sql);
        
        stm.close();
        stm = null;
        cnt.close();
        cnt = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    public static void Update(Projet p) throws Exception{
        Projet ds = (Projet) p;
        String sql = "update projet set date_debut=?, date_fin=?, intitule=? where id=?";
                
        Connection cnt = cxn_statement.getConnection();
        PreparedStatement stm = cxn_statement.getStatement(cnt, sql);
        stm.setString(1, ds.getDate_debut());
        stm.setString(2, ds.getDate_fin());
        stm.setString(3, ds.getIntitule());
        stm.setInt(4, ds.getId());
       
        stm.executeUpdate();
        
        stm.close();
        stm = null;
        cnt.close();
        cnt = null;
        

    }
    public static ArrayList<Projet> GetAll(String Login) throws Exception{
        
     ArrayList<Projet> listeModel = new ArrayList<>();
            String sql = "SELECT p.* FROM projet p,encadrant e where p.encadrant_interne=e.id and e.login= '"+Login+"'";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Projet ds;
            
        while (rs.next()) {
          ds = new Projet(rs.getInt("ID"),rs.getString("intitule"), rs.getString("date_debut"), rs.getString("date_fin"));
                 listeModel.add(ds);
                 
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
            if(listeModel.size()>0)
            {return listeModel;
            }else{listeModel.clear();
                return listeModel;}
            
    }
    public static Projet GetByID(int id) throws Exception{
        
     Projet p = (Projet)Hibernate_Config.Hibernat_Session().get(Projet.class, id);
     return p;
    }
    public static ArrayList<Projet> Get_by_Nom(String s,String Login) throws Exception {
        ArrayList<Projet> listeModel = new ArrayList<>();
            String sql = "SELECT p.* FROM projet p,encadrant e where p.encadrant_interne=e.id and e.login='"+Login+"' and p.intitule like '%"+s+"%' ";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Projet ds;
        while (rs.next()) {
          ds = new Projet(rs.getInt("ID"),rs.getString("intitule"), rs.getString("date_debut"), rs.getString("date_fin"));
                 listeModel.add(ds);
                 
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
    
    //-----------------
    public Projet GetByIntitule(String intitule) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();  
     Query query = Session.createQuery("from Etudiant_Projet where intitule = :intitule");
     query.setParameter("intitule", intitule);
     this.projets = (ArrayList<Projet>)query.list();
     Tran.commit();
     Session.close();
     if(this.projets.isEmpty()){
         return null;
     }else
     return this.projets.get(0);
    }
    
    public  ArrayList<Etudiant_Projet> GetEtudiantProjet(Projet p,Etudiant et) throws Exception{
     ArrayList<Etudiant_Projet> ET_projet = new ArrayList<>();
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from Etudiant_Projet where projet = :projet and etudiant = :et");
     query.setParameter("projet",p.getId());
     query.setParameter("et",et.getId());
     ET_projet = (ArrayList<Etudiant_Projet>)query.list();
     Tran.commit();
     Session.close();
     return ET_projet;
    }
    private  ArrayList<Etudiant_Projet> GetEtudiantProjet(Projet p) throws Exception{
     ArrayList<Etudiant_Projet> ET_projet = new ArrayList<>();
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from Etudiant_Projet where projet = :projet");
     query.setParameter("projet",p.getId());
     ET_projet = (ArrayList<Etudiant_Projet>)query.list();
     Tran.commit();
     Session.close();
     return ET_projet;
    }
     public  ArrayList<Etudiant> GetEtudiantsFromProjet(Projet p) throws Exception{
     ArrayList<Etudiant> ls = new ArrayList<>();
     for (Etudiant_Projet et_projet : GetEtudiantProjet(p) ) {
            ls.add(et_projet.getEtudiant()) ;
         }
     return ls;
    }
     
public static ArrayList<SPE> Get_Info_Prj(String Login , int idprj ) throws Exception{
        
     ArrayList<SPE> listeModel = new ArrayList<>();
            String sql = "SELECT et.nom , et.prenom ,count(s.id) \n" +
"FROM Etudiant_Projet ep,projet p,etudiant et,encadrant e ,info_etudiant ie ,seance s\n" +
"where et.id=ep.etudiant \n" +
"and ep.projet=p.id \n" +
"and ie.etudiant_projet=ep.id \n" +
"and s.id=ie.seance\n" +
"and p.encadrant_interne=e.id \n" +
"and e.login='"+Login+"'\n" +
"and s.isAbscent =true\n" +
"and p.id="+idprj+"\n" +
"GROUP by et.nom , et.prenom";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            SPE ds;
        while (rs.next()) {
          ds = new SPE(rs.getString(1),rs.getString(2),rs.getInt(3));
                 listeModel.add(ds);
                 
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }

    public static ArrayList<String> Get_nb_Seance_prj(String Login ,int idprj) throws Exception{
        
     ArrayList<String> listeModel = new ArrayList<>();
            String sql = "SELECT  count(s.id) FROM Etudiant_Projet ep,projet p,etudiant et,encadrant e ,info_etudiant ie ,seance s where et.id=ep.etudiant and ep.projet=p.id and ie.etudiant_projet=ep.id and s.id=ie.seance and p.encadrant_interne=e.id and e.login='"+Login+"' and p.id="+idprj+" GROUP by et.id";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
           
        while (rs.next()) {
          
                 listeModel.add(""+rs.getInt(1));
                 
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
}
