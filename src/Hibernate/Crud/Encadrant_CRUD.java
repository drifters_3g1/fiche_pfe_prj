/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.Encadrant;
import Hibernate.entite.Etudiant;
import cnx_config.cxn_statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Surface
 */
public class Encadrant_CRUD {

    private SessionFactory sessionFact;
    public ArrayList<Encadrant> liste_encadrant = new ArrayList<>();

    
    public static void ajouter_encadrant(Encadrant encadrant) {
        try {
            Hibernate_Config.Hibernat_Session().save(encadrant);
            Hibernate_Config.Hibernat_Transaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    // constructor for session and transaction
    /*public Encadrant_CRUD(){
        Configuration config = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                applySettings(config.getProperties());
        this.sessionFact = config.buildSessionFactory(builder.build());
    }
    
    public void ajouter_encadrant(Encadrant encadrant) throws Exception{
        Session session = this.sessionFact.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(encadrant);
        transaction.commit();
    }*/
    
    // get login/nickname and password.
    public static ArrayList<Encadrant> getByNickname_password(String login , String pass) throws Exception{
        ArrayList<Encadrant> liste = new ArrayList<>();
        String sql = "SELECT enc.* FROM encadrant enc where enc.login like '%"+login+"%' "
                + "and enc.password Like '%"+pass+"%' ";
        Connection connection = cxn_statement.getConnection();
        Statement stm = cxn_statement.getStatement(connection);
        ResultSet rs = stm.executeQuery(sql);
        Encadrant ds;
        while(rs.next()){
            ds = new Encadrant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("email"),
            rs.getString("tel"),rs.getString("login"),rs.getString("password"));
            liste.add(ds);
        }
        rs.close();
        rs = null;
        stm.close();
        stm = null;
        connection.close();
        connection = null;
        return liste;
    }
    
    public static ArrayList<Encadrant> getByNickname(String login) throws Exception{
        ArrayList<Encadrant> liste = new ArrayList<>();
        String sql = "SELECT enc.* FROM encadrant enc where enc.login like '%"+login+"%' ";
        Connection connection = cxn_statement.getConnection();
        Statement stm = cxn_statement.getStatement(connection);
        ResultSet rs = stm.executeQuery(sql);
        Encadrant ds;
        while(rs.next()){
            ds = new Encadrant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("email"),
            rs.getString("tel"),rs.getString("login"),rs.getString("password"));
            liste.add(ds);
        }
        rs.close();
        rs = null;
        stm.close();
        stm = null;
        connection.close();
        connection = null;
        return liste;
    }
    
    public static void delete_encadrant(Encadrant encadrant) {
        try {
            
           Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.delete(encadrant);
            session.beginTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /*public void Remove(int id) {
        try {
            Hibernate_Config.Hibernat_Session().createQuery("delete from Encadrant where id=" + id).executeUpdate();
            Hibernate_Config.Hibernat_Session().beginTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

    /*public void Update(Encadrant encadrant) {
        try {
            Encadrant enc = (Encadrant) Hibernate_Config.Hibernat_Session().get(Encadrant.class, id);
            Hibernate_Config.Hibernat_Session().update(enc);
            Hibernate_Config.Hibernat_Session().beginTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/
    
    // encadrant update code
    public static void modifier_encadrant(Encadrant encadrant) {
        try {
            Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.update(encadrant);
            session.beginTransaction().commit();
            //session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Encadrant> GetALL() {
        try {
            Query query = Hibernate_Config.Hibernat_Session().createQuery("from Encadrant");
            this.liste_encadrant = (ArrayList<Encadrant>) query.list();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return this.liste_encadrant;
    }
public static ArrayList<Encadrant> Get_cmb_encadrant() throws Exception {
        ArrayList<Encadrant> listeModel = new ArrayList<>();
            String sql = "SELECT e.* FROM encadrant e ";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Encadrant ds;
        while (rs.next()) {
          ds = new Encadrant(rs.getInt("ID"),rs.getString("NOM"), rs.getString("PRENOM"));
                 listeModel.add(ds);
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
    public Encadrant GetByID(int id) throws Exception {

        Encadrant enc = (Encadrant) Hibernate_Config.Hibernat_Session().get(Encadrant.class, id);
        return enc;
    }

}
