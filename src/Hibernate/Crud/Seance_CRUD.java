/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.Etudiant;
import Hibernate.entite.Info_etudiant;
import Hibernate.entite.Projet;
import Hibernate.entite.Seance;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mehdi Aoussar
 */
public class Seance_CRUD {
    
    public  ArrayList<Seance> Seances = new ArrayList<>();
    
    public static void insert(Seance s) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.save(s);
     Tran.commit();
     Session.close();
     
    }
    public static void Delete(int id) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.delete(Session.get(Seance.class,id));
     Tran.commit();
     Session.close();
    }
    public static void Update(Seance s) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.update(s);
     Tran.commit();
     Session.close();
    }
    public  ArrayList<Seance> GetAll() throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from seance");
     this.Seances = (ArrayList<Seance>) query.list();
     Tran.commit();
     Session.close();
     return this.Seances;
    }
    public static Seance GetByID(int id) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
      Seance s = (Seance)Session.get(Seance.class, id);
     Tran.commit();
     Session.close();  
     return s;
    }
    public  ArrayList<Seance> GetAll(Projet p,Etudiant et) throws Exception{
     ArrayList<Info_etudiant> ls = new ArrayList<>();
     ArrayList<Seance> listeS = new ArrayList<>();
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from Info_etudiant where etudiant = :et and projet = :p ");
     query.setParameter("et", et.getId());
     query.setParameter("p", p.getId());
     ls = (ArrayList<Info_etudiant>) query.list();
     if(ls.size()>0){
         for (Info_etudiant l : ls) {
             listeS.add(l.getSeance());
         }
     }
     Tran.commit();
     Session.close();
     return listeS;
    }
   
}

