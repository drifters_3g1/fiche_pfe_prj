/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import java.util.ArrayList;
import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.Etudiant_Projet;
import cnx_config.cxn_statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Surface
 */
public class Etudiant_Projet_CRUD {
    public static ArrayList<Etudiant_Projet> projets = new ArrayList<>();
    
    public static void insert(Etudiant_Projet p) {

        try {
            Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.save(p);
            session.beginTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    public static void Delete(Etudiant_Projet p) {

          try {
           Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.delete(p);
            session.beginTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    public static void Update(Etudiant_Projet p) {

          try {
           Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.update(p);
            session.beginTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
