/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.Livrable;
import Hibernate.entite.Seance;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mehdi Aoussar
 */
public class Livrable_CRUD {
     public  ArrayList<Livrable> Livrables = new ArrayList<>();
    
    public static void insert(Livrable l) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.save(l);
     Tran.commit();
     Session.close();
     
    }
    public static void Delete(int id) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.delete(Session.get(Livrable.class,id));
     Tran.commit();
     Session.close();
    }
    public static void Update(Livrable l) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.update(l);
     Tran.commit();
     Session.close();
    }
    public  ArrayList<Livrable> GetAll() throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from livrable");
     this.Livrables = (ArrayList<Livrable>) query.list();
     Tran.commit();
     Session.close();
     return this.Livrables;
    }
    public static Livrable GetByID(int id) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
      Livrable l = (Livrable)Session.get(Seance.class, id);
     Tran.commit();
     Session.close();  
     return l;
    } 
     public  ArrayList<Livrable> GetBySeance(Seance s) throws Exception{
     ArrayList<Livrable> ls = new ArrayList<>();
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from livrable where seance = :seance");
     query.setParameter("seance",s.getId());
     ls = (ArrayList<Livrable>) query.list();
     Tran.commit();
     Session.close();  
     return ls;
    } 
}
