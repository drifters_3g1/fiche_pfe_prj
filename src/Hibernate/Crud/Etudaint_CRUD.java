/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.hibernate.Query;
import cnx_config.cxn_statement;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


/**
 *
 * @author Sallaoui Hassan
 */
public class Etudaint_CRUD {

      public static ArrayList<Etudiant> liste_etudiant ;
      
    public static void Ajouter(Etudiant e) {
        try {
            Hibernate_Config.Hibernat_Session().save(e);
            Hibernate_Config.Hibernat_Transaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void Remove(Etudiant e) {
        try {
            
           Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.delete(e);
            session.beginTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void Update(Etudiant e) {
        try {
            Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session = sessionfactory.openSession();
            session.update(e);
            session.beginTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Etudiant> GetALL() {
        try {
            Query query = Hibernate_Config.Hibernat_Session().createQuery("from Etudiant");
            liste_etudiant = (ArrayList<Etudiant>) query.list();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return liste_etudiant;
    }
   public static ArrayList<Etudiant> Get_by_encadrant() throws Exception {
        ArrayList<Etudiant> listeModel = new ArrayList<>();
            String sql = "SELECT e.* FROM etudiant e ";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Etudiant ds;
        while (rs.next()) {
          ds = new Etudiant(rs.getInt("ID"),rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("Email"),rs.getString("TEL") );
                 listeModel.add(ds);
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
 public static ArrayList<Etudiant> Get_by_Nom(String s) throws Exception {
        ArrayList<Etudiant> listeModel = new ArrayList<>();
            String sql = "SELECT e.* FROM etudiant e where e.nom like '%"+s+"%' ";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Etudiant ds;
        while (rs.next()) {
          ds = new Etudiant(rs.getInt("ID"),rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("Email"),rs.getString("TEL") );
                 listeModel.add(ds);
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
 public static ArrayList<Etudiant> Get_by_Prenom(String s) throws Exception {
        ArrayList<Etudiant> listeModel = new ArrayList<>();
            String sql = "SELECT e.* FROM etudiant e where e.prenom like '%"+s+"%' ";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Etudiant ds;
        while (rs.next()) {
          ds = new Etudiant(rs.getInt("ID"),rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("Email"),rs.getString("TEL") );
                 listeModel.add(ds);
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
 public static ArrayList<Etudiant> Get_by_Nom_Prenom(String N,String P) throws Exception {
        ArrayList<Etudiant> listeModel = new ArrayList<>();
            String sql = "SELECT e.* FROM etudiant e where e.prenom like '%"+P+"%' and e.nom Like '%"+N+"%' ";
            Connection cnt = cxn_statement.getConnection();
            Statement stm = cxn_statement.getStatement(cnt);
            ResultSet rs = stm.executeQuery(sql);
            Etudiant ds;
        while (rs.next()) {
          ds = new Etudiant(rs.getInt("ID"),rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("Email"),rs.getString("TEL") );
                 listeModel.add(ds);
            }
            rs.close();
            rs = null;
            stm.close();
            stm = null;
            cnt.close();
            cnt = null;
        return listeModel;
    }
   public static Etudiant GetByID(int id) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
      Etudiant et = (Etudiant)Session.get(Etudiant.class, id);
     Tran.commit();
     Session.close();  
     return et;
    }

}
