/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Crud;

import Hibernate.Config.Hibernate_Config;
import Hibernate.entite.Etudiant;
import Hibernate.entite.Etudiant_Projet;
import Hibernate.entite.Info_etudiant;
import Hibernate.entite.Projet;
import Hibernate.entite.Seance;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mehdi Aoussar
 */
public class infoEtudiant_CRUD {
    
     public  ArrayList<Info_etudiant> liste = new ArrayList<>();
    
    public static void insert(Info_etudiant s) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.save(s);
     Tran.commit();
     Session.close();
     
    }
    public static void Delete(int id) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.delete(Session.get(Info_etudiant.class,id));
     Tran.commit();
     Session.close();
    }
    public static void Update(Info_etudiant s) throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Session.update(s);
     Tran.commit();
     Session.close();
    }
    public  ArrayList<Info_etudiant> GetAll() throws Exception{
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from seance");
     this.liste = (ArrayList<Info_etudiant>) query.list();
     Tran.commit();
     Session.close();
     return this.liste;
    }
     public Info_etudiant GetinfoEtudiant(Projet p,Etudiant et,Seance s) throws Exception{
     ArrayList<Info_etudiant> ls = new ArrayList<>();
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from Info_etudiant where etudiant = :et and projet = :p and seance = :s ");
     query.setParameter("et", et.getId());
     query.setParameter("p", p.getId());
     query.setParameter("s", s.getId());
     ls = (ArrayList<Info_etudiant>) query.list();
     Tran.commit();
     Session.close(); 
        if(ls.isEmpty()){
             return null;
        }else
            return ls.get(0);
    }

     public static int GetEngagement(Projet p,Etudiant et,Seance s) throws Exception{
     ArrayList<Info_etudiant> ls = new ArrayList<>();
     ArrayList<Etudiant_Projet> lst = new ArrayList<>();
     Etudiant_Projet ep;
     Session Session = Hibernate_Config.GetSessionFactory().openSession();
     Transaction Tran = Session.beginTransaction();
     Query query = Session.createQuery("from Etudiant_Projet where projet = :projet and etudiant = :et");
     query.setParameter("projet",p.getId());
     query.setParameter("et",et.getId());
     lst = (ArrayList<Etudiant_Projet>)query.list();
     if(!lst.isEmpty()){
         ep = lst.get(0);
         Query req = Session.createQuery("from Info_etudiant where etudiant_projet = :ep and seance = :s ");
         query.setParameter("s", s.getId());
         query.setParameter("ep", ep.getId());
         ls = (ArrayList<Info_etudiant>) query.list();
         Tran.commit();
         Session.close(); 
    }
     if(ls.isEmpty()){
            return -1;
         }else
        return ls.get(0).getEngagement();
     }
}
