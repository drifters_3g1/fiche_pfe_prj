/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.Config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Sallaoui Hassan
 */
public class Hibernate_Config {
    private static Session session ;
    private static Transaction transaction;
    private static SessionFactory SessionFactory;
    
    public static Session Hibernat_Session()
    {
        if(session == null){
        Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        session = sessionfactory.openSession();
        }
       return session;
    }
    public static Transaction Hibernat_Transaction()
    {
        if(transaction == null){
         transaction = Hibernat_Session().beginTransaction();
        }
        return transaction;
    }
     public static SessionFactory GetSessionFactory()
    {
     if(SessionFactory == null){
        Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory = configuration.buildSessionFactory(builder.build());
        
        }
       return SessionFactory;   
    }
    
}
