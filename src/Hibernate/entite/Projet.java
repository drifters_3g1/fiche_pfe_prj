/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;

import java.time.LocalDate;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Sohail Erzini
 */

@Entity
@Table (name="projet")
public class Projet {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")        
    private int id;
    
    @Column(name="intitule", unique = true) 
    private String intitule;
    
    @Column(name="date_debut") 
    private String date_debut;
    
    @Column(name="date_fin") 
    private String date_fin;

    @ManyToOne(fetch = FetchType.LAZY , optional = false)
    @JoinColumn(name = "encadrant_interne")
    private Encadrant encadrant_interne;
    
    @ManyToOne(fetch = FetchType.LAZY , optional = true)
    @JoinColumn(name = "encadrant_externe")
    private Encadrant encadrant_externe;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public Encadrant getEncadrant_interne() {
        return encadrant_interne;
    }

    public void setEncadrant_interne(Encadrant encadrant_interne) {
        this.encadrant_interne = encadrant_interne;
    }

    public Encadrant getEncadrant_externe() {
        return encadrant_externe;
    }

    public void setEncadrant_externe(Encadrant encadrant_externe) {
        this.encadrant_externe = encadrant_externe;
    }

    public Projet(int id,String intitule, String date_debut, String date_fin, Encadrant encadrant_interne, Encadrant encadrant_externe) {
        this.id = id;
        this.intitule = intitule;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.encadrant_interne = encadrant_interne;
        this.encadrant_externe = encadrant_externe;
    }
    public Projet(int id,String intitule, String date_debut, String date_fin) {
        this.id = id;
        this.intitule = intitule;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
       
    }

    public Projet() {
    }
}
