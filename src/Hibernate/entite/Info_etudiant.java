/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;
import java.io.Serializable;
import javax.persistence.*;
/**
 *
 * @author Sallaoui Hassan
 */
@Entity
@Table (name="Info_etudiant")
public class Info_etudiant implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name ="id")
    private int id;
    
    @Column(name="Engagement")
    private int Engagement;
    
    @ManyToOne(fetch = FetchType.LAZY , optional = false)
    @JoinColumn(name = "etudiant_projet")
    private Etudiant_Projet etudiant_projet;
   
    @ManyToOne(fetch = FetchType.LAZY , optional = false)
    @JoinColumn(name = "seance")
    private Seance seance;

    public Info_etudiant(int Engagement, Etudiant_Projet etudiant_projet, Seance seance) {
        this.Engagement = Engagement;
        this.etudiant_projet = etudiant_projet;
        this.seance = seance;
    }
    
    public Info_etudiant(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEngagement() {
        return Engagement;
    }

    public void setEngagement(int Engagement) {
        this.Engagement = Engagement;
    }

    public Etudiant_Projet getEtudiant_projet() {
        return etudiant_projet;
    }

    public void setEtudiant_projet(Etudiant_Projet etudiant_projet) {
        this.etudiant_projet = etudiant_projet;
    }

    public Seance getSeance() {
        return seance;
    }

    public void setSeance(Seance seance) {
        this.seance = seance;
    }
    
    

}
