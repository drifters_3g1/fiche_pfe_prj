/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;

/**
 *
 * @author Sallaoui Hassan
 */
public class SPE {
    
    String nom;
    String prenom;
    int count;

    public SPE(String nom, String prenom, int count) {
        this.nom = nom;
        this.prenom = prenom;
        this.count = count;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    
}
