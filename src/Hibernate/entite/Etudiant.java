/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;
import java.io.Serializable;
import javafx.scene.control.ListCell;
import javax.persistence.*;
/**
 *
 * @author Sallaoui Hassan
 */
@Entity
@Table (name="etudiant")
public class Etudiant implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")        
    private int id;
    
    @Column(name="nom")
    private String nom;
    
    @Column(name="prenom")
    private String prenom;
    
    @Column(name="email" , unique = true)
    private String email;
    
    @Column(name="tel")
    private String tel;

    public Etudiant(int id, String nom, String prenom, String email, String tel) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
    }

    public Etudiant() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }  
    @Override
    public String toString()
    {
     return this.getNom()+" "+this.getPrenom();
    }
    
    
}
