/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;

import java.io.Serializable;
import javax.persistence.*;
/**
 *
 * @author Sallaoui Hassan
 * EL gnaoui med amine update
 */
@Entity
@Table (name="Etudiant_Projet")
public class Etudiant_Projet implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    @ManyToOne(fetch = FetchType.LAZY , optional = false)
    @JoinColumn(name = "etudiant")
    private Etudiant etudiant;
    
    @ManyToOne(fetch = FetchType.LAZY , optional = false)
    @JoinColumn(name = "projet")
    private Projet projet;

    public Etudiant_Projet(int id,Etudiant etudiant, Projet projet) {
        this.id = id;
        this.etudiant = etudiant;
        this.projet = projet;
    }

    public Etudiant_Projet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }
}
