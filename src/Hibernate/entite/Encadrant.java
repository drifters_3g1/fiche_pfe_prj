/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author Sohail Erzini
 */

@Entity
@Table (name="encadrant")
public class Encadrant implements Serializable {
 
    // Encadrant Attributes.
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")        
    private int id ;
    
    @Column(name="nom")
    private String nom;
    
    @Column(name="prenom")
    private String prenom;
    
    @Column(name="email", unique = true)
    private String email;
    
    @Column(name="tel")
    private String tel;
    
    @Column(name="login") // nickname of the user
    private String login;
    
    @Column(name="password") // nickname of the user
    private String password;
    
   

    public Encadrant(String nom, String prenom, String email, String tel, String login, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
        this.login = login;
        this.password = password;
    }
    public Encadrant(int id,String nom, String prenom, String email, String tel, String login, String password) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
        this.login = login;
        this.password = password;
    }
    public Encadrant(int id,String nom, String prenom) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
    }
    // empty constructor
    public Encadrant (){
        
    }
    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getTel() {
        return tel;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    
    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    @Override
    public String toString()
    {
     return this.getNom()+" "+this.getPrenom();
    }
}
