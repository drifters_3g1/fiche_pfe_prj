/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Med amine el gnaoui
 */
@Entity
@Table (name="seance")
public class Seance implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")        
    private int id;
    
    @Column(name="num_seance") 
    private int num_seance;
    
    @Column(name="commentaire" , columnDefinition = "text") 
    private String commentaire;
    
    @Column(name="todo" , columnDefinition = "text") 
    private String todo;
    
    @Column(name="isAbscent") 
    private boolean isAbscent;
    
    @Column(name="date_seance") 
    private String date_seance;
    
    @Column(name="heure_debut") 
    private String heure_debut;
    
    @Column(name="heure_fin") 
    private String heure_fin;
    
    @OneToMany(cascade = CascadeType.ALL , fetch = FetchType.LAZY , mappedBy = "seance")
    private List<Livrable> livrables;

    
    public Seance() {
    }

    public Seance(int num_seance, String commentaire, String todo, boolean isAbscent, String date_seance, String heure_debut, String heure_fin) {
        this.num_seance = num_seance;
        this.commentaire = commentaire;
        this.todo = todo;
        this.isAbscent = isAbscent;
        this.date_seance = date_seance;
        this.heure_debut = heure_debut;
        this.heure_fin = heure_fin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum_seance() {
        return num_seance;
    }

    public void setNum_seance(int num_seance) {
        this.num_seance = num_seance;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public boolean isIsAbscent() {
        return isAbscent;
    }

    public void setIsAbscent(boolean isAbscent) {
        this.isAbscent = isAbscent;
    }

    public String getDate_seance() {
        return date_seance;
    }

    public void setDate_seance(String date_seance) {
        this.date_seance = date_seance;
    }

    public String getHeure_debut() {
        return heure_debut;
    }

    public void setHeure_debut(String heure_debut) {
        this.heure_debut = heure_debut;
    }

    public String getHeure_fin() {
        return heure_fin;
    }

    public void setHeure_fin(String heure_fin) {
        this.heure_fin = heure_fin;
    }

    public List<Livrable> getLivrables() {
        return livrables;
    }

    public void setLivrables(List<Livrable> livrables) {
        this.livrables = livrables;
    }

}
