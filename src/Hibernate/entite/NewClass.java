/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate.entite;

import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Sallaoui Hassan
 */

public class NewClass {
    // Classe Pour Cree La base de donner 
    public static void main(String[] args) {
        Configuration configuration= new Configuration().configure();
        StandardServiceRegistryBuilder builder =new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionfactory=configuration.buildSessionFactory(builder.build());
        Session session=sessionfactory.openSession();
        Transaction transaction=session.beginTransaction();
    }
}
