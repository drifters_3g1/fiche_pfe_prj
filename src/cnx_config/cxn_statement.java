/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnx_config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author Sallaoui Hassan
 */
public class cxn_statement {
    private static final String DBNAME;
    private static final String URL;
    private static final String USERNAME;
    private static final String DRIVER;

    static {
        DBNAME = "db_gsfiche_pfe";
        URL = "jdbc:mysql://localhost:3306/";
        DRIVER = "com.mysql.jdbc.Driver";
        USERNAME = "root";
        
    }
    public static Connection getConnection() throws Exception {
        
        Class.forName(DRIVER);

        return DriverManager.getConnection(URL + DBNAME, USERNAME, null);
    }
    
    public static Statement getStatement(Connection c) throws Exception {
        return c.createStatement();
    }
    
    public static PreparedStatement getStatement(Connection c, String sql) throws Exception {
        return c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    }
}
