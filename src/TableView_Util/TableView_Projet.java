/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableView_Util;

import Hibernate.entite.*;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 *
 * @author Sallaoui Hassan
 */
public class TableView_Projet {
    private TableColumn<Projet, String> getDate_debutColumn() {
        TableColumn<Projet, String> fNameCol = new TableColumn<>("Date_debut");
        fNameCol.setCellValueFactory(new PropertyValueFactory<>("date_debut"));
        return fNameCol;
    }
    private TableColumn<Projet, String> getIntituleColumn() {
        TableColumn<Projet, String> lastNameCol = new TableColumn<>("Intitule");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("intitule"));
        return lastNameCol;
    }
    private TableColumn<Projet, String> getDate_FinColumn() {
        TableColumn<Projet, String> adrCol = new TableColumn<>("Date_Fin");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("date_fin"));
        return adrCol;
    }

    
    protected ArrayList<TableColumn> getColumns() {
        ArrayList<TableColumn> cols = new ArrayList<>();
        cols.add(getIdColumn());
        cols.add(getIntituleColumn());
        cols.add(getDate_debutColumn());
        cols.add(getDate_FinColumn());
        
        return cols;
    }
    protected TableColumn<Projet, Integer> getIdColumn() {
        TableColumn<Projet, Integer> IdCol = new TableColumn<>("Id");
        IdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        return IdCol;
    }
    
    public ObservableList<Projet> getRowsFromArrayList(ArrayList<Projet> al) {
        ObservableList<Projet> liste = FXCollections.<Projet>observableArrayList();
        liste.addAll(al);
        return liste;
    }
    
    public void addRow(TableView tview, Projet am) {
        ObservableList ol = tview.getItems();
        ol.add(am);
        ol = null;
    }

    public void deleteRow(TableView tview) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.remove(index);
        }
        ol = null;
    }

    public void editRow(TableView tview, Projet am) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.set(index, am);
        }
        ol = null;
    }

    public void populate(TableView tview, ArrayList<Projet> al) {
        ObservableList<TableColumn> ol = tview.getColumns();
        if(ol.isEmpty())
            ol.addAll(getColumns());
        tview.setItems(getRowsFromArrayList(al));
        // sert uniquement à definir la largeur des colonnes
            
    }
    
    public Projet getSelectedRow(TableView tview) {
        return (Projet) tview.getSelectionModel().getSelectedItem();
    }
    
    public void empty(TableView tview) {
        ArrayList<Projet> al = new ArrayList();
        ObservableList ol = FXCollections.observableArrayList(al);
        tview.setItems(ol);
        ol = null;
        al = null;
    }
}
