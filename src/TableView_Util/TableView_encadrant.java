/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableView_Util;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import Hibernate.entite.*;

/**
 *
 * @author Sallaoui Hassan
 */
public class TableView_encadrant {
     private TableColumn<Encadrant, String> getFirstNameColumn() {
        TableColumn<Encadrant, String> fNameCol = new TableColumn<>("Prénom");
        fNameCol.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        return fNameCol;
    }
    private TableColumn<Encadrant, String> getLastNameColumn() {
        TableColumn<Encadrant, String> lastNameCol = new TableColumn<>("Nom");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
        return lastNameCol;
    }
    private TableColumn<Encadrant, String> getAdresseColumn() {
        TableColumn<Encadrant, String> adrCol = new TableColumn<>("Adresse");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        return adrCol;
    }
    private TableColumn<Encadrant, String> getTelColumn() {
        TableColumn<Encadrant, String> telCol = new TableColumn<>("Tel");
        telCol.setCellValueFactory(new PropertyValueFactory<>("tel"));
        return telCol;
    }
    private TableColumn<Encadrant, String> getEmailColumn() {
        TableColumn<Encadrant, String> fctCol = new TableColumn<>("EmailC");
        fctCol.setCellValueFactory(new PropertyValueFactory<>("emailC"));
        return fctCol;
    }
    
    protected ArrayList<TableColumn> getColumns() {
        ArrayList<TableColumn> cols = new ArrayList<>();
        cols.add(getIdColumn());
        cols.add(getLastNameColumn());
        cols.add(getFirstNameColumn());
        cols.add(getAdresseColumn());
        cols.add(getTelColumn());
        cols.add(getEmailColumn());
        return cols;
    }
    protected TableColumn<Encadrant, Integer> getIdColumn() {
        TableColumn<Encadrant, Integer> IdCol = new TableColumn<>("Id");
        IdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        return IdCol;
    }
    
    public ObservableList<Encadrant> getRowsFromArrayList(ArrayList<Encadrant> al) {
        ObservableList<Encadrant> liste = FXCollections.<Encadrant>observableArrayList();
        liste.addAll(al);
        return liste;
    }
    
    public void addRow(TableView tview, Encadrant am) {
        ObservableList ol = tview.getItems();
        ol.add(am);
        ol = null;
    }

    public void deleteRow(TableView tview) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.remove(index);
        }
        ol = null;
    }

    public void editRow(TableView tview, Encadrant am) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.set(index, am);
        }
        ol = null;
    }

    public void populate(TableView tview, ArrayList<Encadrant> al) {
        ObservableList<TableColumn> ol = tview.getColumns();
        if(ol.isEmpty())
            ol.addAll(getColumns());
        tview.setItems(getRowsFromArrayList(al));
        // sert uniquement à definir la largeur des colonnes
            tview.setColumnResizePolicy(new Callback<TableView.ResizeFeatures, Boolean>() {
                        @Override
                        public Boolean call(TableView.ResizeFeatures p) {
                            return true;
                        }
                    });
    }
    
    public Encadrant getSelectedRow(TableView tview) {
        return (Encadrant) tview.getSelectionModel().getSelectedItem();
    }
    
    public void empty(TableView tview) {
        ArrayList<Encadrant> al = new ArrayList();
        ObservableList ol = FXCollections.observableArrayList(al);
        tview.setItems(ol);
        ol = null;
        al = null;
    }
}
