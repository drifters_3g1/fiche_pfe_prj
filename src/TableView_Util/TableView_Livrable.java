/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableView_Util;

import Hibernate.entite.*;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 *
 * @author Sallaoui Hassan
 */
public class TableView_Livrable {
    private TableColumn<Livrable, String> getDateColumn() {
        TableColumn<Livrable, String> fNameCol = new TableColumn<>("Date");
        fNameCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        return fNameCol;
    }
    private TableColumn<Livrable, String> getIntituleColumn() {
        TableColumn<Livrable, String> lastNameCol = new TableColumn<>("Intitule");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("intitule"));
        return lastNameCol;
    }
    private TableColumn<Livrable, String> getPourcentageColumn() {
        TableColumn<Livrable, String> adrCol = new TableColumn<>("Pourcentage");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("pourcentage"));
        return adrCol;
    }

    
    protected ArrayList<TableColumn> getColumns() {
        ArrayList<TableColumn> cols = new ArrayList<>();
        cols.add(getIdColumn());
        cols.add(getDateColumn());
        cols.add(getIntituleColumn());
        cols.add(getPourcentageColumn());
        return cols;
    }
    protected TableColumn<Livrable, Integer> getIdColumn() {
        TableColumn<Livrable, Integer> IdCol = new TableColumn<>("Id");
        IdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        return IdCol;
    }
    public void AddColumnsLivrable(TableView tview) {
        if(tview.getColumns().isEmpty())
        tview.getColumns().addAll(getColumns());      
    }
    public ObservableList<Livrable> getRowsFromArrayList(ArrayList<Livrable> al) {
        ObservableList<Livrable> liste = FXCollections.<Livrable>observableArrayList();
        liste.addAll(al);
        return liste;
    }
    
    public void addRow(TableView tview, Livrable am) {
        ObservableList ol = tview.getItems();
        ol.add(am);
        ol = null;
    }

    public void deleteRow(TableView tview) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.remove(index);
        }
        ol = null;
    }

    public void editRow(TableView tview, Livrable am) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.set(index, am);
        }
        ol = null;
    }

    public void populate(TableView tview, ArrayList<Livrable> al) {
        ObservableList<TableColumn> ol = tview.getColumns();
        if(ol.isEmpty())
            ol.addAll(getColumns());
        tview.setItems(getRowsFromArrayList(al));
        // sert uniquement à definir la largeur des colonnes
            tview.setColumnResizePolicy(new Callback<TableView.ResizeFeatures, Boolean>() {
                        @Override
                        public Boolean call(TableView.ResizeFeatures p) {
                            return true;
                        }
                    });
    }
    
    public Livrable getSelectedRow(TableView tview) {
        return (Livrable) tview.getSelectionModel().getSelectedItem();
    }
    
    public void empty(TableView tview) {
        ArrayList<Livrable> al = new ArrayList();
        ObservableList ol = FXCollections.observableArrayList(al);
        tview.setItems(ol);
        ol = null;
        al = null;
    }    
}
