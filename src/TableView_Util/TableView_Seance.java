/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableView_Util;

import Hibernate.Crud.Seance_CRUD;
import Hibernate.entite.Seance;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Mehdi Aoussar
 */
public class TableView_Seance {
    private TableColumn<Seance, Integer> getIdColumn() {
        TableColumn<Seance, Integer> IdCol = new TableColumn<>("Id");
        IdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        return IdCol;
    }
    private TableColumn<Seance, String> getnum_seanceColumn() {
        TableColumn<Seance, String> fNameCol = new TableColumn<>("num_seance");
        fNameCol.setCellValueFactory(new PropertyValueFactory<>("num_seance"));
        return fNameCol;
    }
    private TableColumn<Seance, String> getcommentaireColumn() {
        TableColumn<Seance, String> lastNameCol = new TableColumn<>("commentaire");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("commentaire"));
        return lastNameCol;
    }
    private TableColumn<Seance, String> gettodoColumn() {
        TableColumn<Seance, String> adrCol = new TableColumn<>("todo");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("todo"));
        return adrCol;
    }
    private TableColumn<Seance, Boolean> getisAbscentColumn() {
        TableColumn<Seance, Boolean> adrCol = new TableColumn<>("isAbscent");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("isAbscent"));
        return adrCol;
    }
     private TableColumn<Seance, String> getdate_seanceColumn() {
        TableColumn<Seance, String> lastNameCol = new TableColumn<>("date_seance");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("date_seance"));
        return lastNameCol;
    }
    private TableColumn<Seance, String> getheure_debutColumn() {
        TableColumn<Seance, String> adrCol = new TableColumn<>("heure_debut");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("heure_debut"));
        return adrCol;
    }
    private TableColumn<Seance, String> getheure_finColumn() {
        TableColumn<Seance, String> adrCol = new TableColumn<>("heure_fin");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("heure_fin"));
        return adrCol;
    }
    
    protected ArrayList<TableColumn> getColumns() {
        ArrayList<TableColumn> cols = new ArrayList<>();
        cols.add(getIdColumn());
        cols.add(getnum_seanceColumn());
        cols.add(getcommentaireColumn());
        cols.add(gettodoColumn());
        cols.add(getisAbscentColumn());
        cols.add(getdate_seanceColumn());
        cols.add(getheure_debutColumn());
        cols.add(getheure_finColumn());
        return cols;
    }
    
    public void AddColumnsSeance(TableView tview) {
        if(tview.getColumns().isEmpty())
        tview.getColumns().addAll(getColumns());      
    }
   
    public ObservableList<Seance> getRowsSeance(ArrayList<Seance> Seances) throws Exception{
        ObservableList<Seance> liste = FXCollections.<Seance>observableArrayList();
        liste.addAll(Seances);
        return liste;  
    }
    
    public void addRow(TableView tview, Seance S) {
        ObservableList ol = tview.getItems();
        ol.add(S);
        ol = null;
    }

    public void deleteRow(TableView tview) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.remove(index);
        }
        ol = null;
    }

    public void editRow(TableView tview, Seance S) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.set(index, S);
        }
        ol = null;
    }

    
    
    public Seance getSelectedRow(TableView tview) {
        return (Seance) tview.getSelectionModel().getSelectedItem();
    }
    
    public void empty(TableView tview) {
        ArrayList<Seance> al = new ArrayList();
        ObservableList ol = FXCollections.observableArrayList(al);
        tview.setItems(ol);
        ol = null;
        al = null;
    }
}
