/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableView_Util;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import Hibernate.entite.Etudiant;
/**
 *
 * @author Sallaoui Hassan
 */
public class TableView_etudiant {
     private TableColumn<Etudiant, String> getFirstNameColumn() {
        TableColumn<Etudiant, String> fNameCol = new TableColumn<>("Prénom");
        fNameCol.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        return fNameCol;
    }
    private TableColumn<Etudiant, String> getLastNameColumn() {
        TableColumn<Etudiant, String> lastNameCol = new TableColumn<>("Nom");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
        return lastNameCol;
    }
    private TableColumn<Etudiant, String> getAdresseColumn() {
        TableColumn<Etudiant, String> adrCol = new TableColumn<>("Adresse");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        return adrCol;
    }
    private TableColumn<Etudiant, String> getTelColumn() {
        TableColumn<Etudiant, String> telCol = new TableColumn<>("Tel");
        telCol.setCellValueFactory(new PropertyValueFactory<>("tel"));
        return telCol;
    }
    private TableColumn<Etudiant, String> getEmailColumn() {
        TableColumn<Etudiant, String> fctCol = new TableColumn<>("Email");
        fctCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        return fctCol;
    }
    
    protected ArrayList<TableColumn> getColumns() {
        ArrayList<TableColumn> cols = new ArrayList<>();
        cols.add(getIdColumn());
        cols.add(getLastNameColumn());
        cols.add(getFirstNameColumn());
        cols.add(getTelColumn());
        cols.add(getEmailColumn());
        return cols;
    }
    protected TableColumn<Etudiant, Integer> getIdColumn() {
        TableColumn<Etudiant, Integer> IdCol = new TableColumn<>("Id");
        IdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        return IdCol;
    }
    
    public ObservableList<Etudiant> getRowsFromArrayList(ArrayList<Etudiant> al) {
        ObservableList<Etudiant> liste = FXCollections.<Etudiant>observableArrayList();
        liste.addAll(al);
        return liste;
    }
    
    public void addRow(TableView tview, Etudiant am) {
        ObservableList ol = tview.getItems();
        ol.add(am);
        ol = null;
    }

    public void deleteRow(TableView tview) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.remove(index);
        }
        ol = null;
    }

    public void editRow(TableView tview, Etudiant am) {
        ObservableList ol = tview.getItems();
        int index = tview.getSelectionModel().getSelectedIndex();
        if (index >= 0 && index < ol.size()) {
            ol.set(index, am);
        }
        ol = null;
    }

    public void populate(TableView tview, ArrayList<Etudiant> al) {
        ObservableList<TableColumn> ol = tview.getColumns();
        if(ol.isEmpty())
            ol.addAll(getColumns());
        tview.setItems(getRowsFromArrayList(al));
        // sert uniquement à definir la largeur des colonnes
        
    }
    
    public Etudiant getSelectedRow(TableView tview) {
        return (Etudiant) tview.getSelectionModel().getSelectedItem();
    }
    
    public void empty(TableView tview) {
        ArrayList<Etudiant> al = new ArrayList();
        ObservableList ol = FXCollections.observableArrayList(al);
        tview.setItems(ol);
        ol = null;
        al = null;
    }
}
