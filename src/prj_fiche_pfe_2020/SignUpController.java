/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prj_fiche_pfe_2020;

import Hibernate.Crud.Encadrant_CRUD;
import Hibernate.entite.Encadrant;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class SignUpController implements Initializable {

    @FXML
    private JFXTextField signup_nickname_tb;
    @FXML
    private JFXTextField signup_email_tb;
    @FXML
    private JFXTextField signup_password_tb;
    @FXML
    private JFXTextField signup_nom_tb;
    @FXML
    private JFXTextField signup_prenom_tb;
    @FXML
    private JFXTextField signup_tele_tb;
    @FXML
    private JFXButton btn_add_encadrant;
    @FXML
    private JFXButton navig_login;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void add_encadrant_click(javafx.scene.input.MouseEvent event){
        if (event.getSource() == btn_add_encadrant){
            if(signup_nickname_tb.getText()!="" && signup_email_tb.getText()!="" 
                    && signup_password_tb.getText()!="" && signup_nom_tb.getText()!="" &&
                    signup_prenom_tb.getText()!="" && signup_tele_tb.getText()!=""
                    ){
                        Encadrant_CRUD.ajouter_encadrant(new Encadrant(signup_nom_tb.getText(),
                                signup_prenom_tb.getText(), signup_email_tb.getText(),signup_tele_tb.getText()
                        ,signup_nickname_tb.getText() , signup_password_tb.getText()));
                        // clear textfields
                        signup_nickname_tb.setText("");
                        signup_email_tb.setText("");
                        signup_password_tb.setText("");
                        signup_nom_tb.setText("");
                        signup_prenom_tb.setText("");
                        signup_tele_tb.setText("");  
            }
        }
    }
    // navigate from signup scene to loginscene
    @FXML
    public void returntoLogin_button_clicked(ActionEvent event) throws IOException{
        Parent loginParent = FXMLLoader.load(getClass().getResource("Login.fxml"));
        Scene loginScene = new Scene(loginParent);
        
        // gets stage info code
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(loginScene);
        window.show();
    }
}
