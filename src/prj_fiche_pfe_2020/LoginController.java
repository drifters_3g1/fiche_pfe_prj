/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prj_fiche_pfe_2020;

import Hibernate.Crud.Encadrant_CRUD;
import Hibernate.entite.Encadrant;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Sohail Erzini
 */
public class LoginController implements Initializable {

    @FXML
    private JFXButton btn_signIn;
    @FXML
    private JFXTextField login_nick_tb;
    @FXML
    private JFXPasswordField login_pass_tb;
    @FXML
    private Label login_error_lbl;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
       // method to navigate from loginscene to main sceneand pass encadrant object.
    @FXML
    public void signIn_button_clicked(ActionEvent event) throws IOException, Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Main.fxml"));
        Parent MainParent = loader.load();
        Scene MainScene = new Scene(MainParent);
        // access main scene controller and call the method init
        MainController controller = loader.getController();
        // gets stage info code
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        
        if(event.getSource() == btn_signIn){
            if(login_nick_tb.getText() !="" && login_pass_tb.getText() !=""){
                String nickname = login_nick_tb.getText();
            String pass = login_pass_tb.getText();
            ArrayList<Encadrant> liste = Encadrant_CRUD.getByNickname_password(nickname, pass);
            for (Encadrant encadrant : liste) {
                String login = encadrant.getLogin();
                String password = encadrant.getPassword();
                if(login_nick_tb.getText().equals(login)  && login_pass_tb.getText().equals(password)){
                    // initdata returns encadrant obj
                    controller.initEncadrantData(encadrant);
                    window.setScene(MainScene);
                    window.show();
            }else{
                    login_error_lbl.setText("Username/password incorrect");
                }
            }
            }
        }  
    }

    
    // method to navigate from loginscene to signup scene.
    @FXML
    public void signUp_button_clicked(ActionEvent event) throws IOException{
        Parent signUpParent = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
        Scene signUpScene = new Scene(signUpParent);
        
        // gets stage info code
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(signUpScene);
        window.show();
    }
}
