/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prj_fiche_pfe_2020;
import Hibernate.entite.*;
import Hibernate.Crud.*;
import TableView_Util.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.sun.glass.events.MouseEvent;
import java.io.IOException;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.StringConverter;



/**
 * FXML Controller class
 *
 * @author Sohail Erzini
 */
public class MainController implements Initializable {
    
    private Scene scene;
    private FXMLLoader loader;
    private Etudiant selected_etudiant;
     private Projet selected_projet;
     private Etudiant_Projet  selected_etudiant_projet;
    private static Encadrant authentified_encadrant;
    @FXML
    private Pane pnl_encadrant;
    @FXML
    private Pane pnl_projet;
    @FXML
    private Pane pnl_etudiant;
    @FXML
    private Pane pnl_seance;
    @FXML
    private JFXButton btn_encadrant;
    @FXML
    private JFXButton btn_projet;
    @FXML
    private JFXButton btn_etudiant;
    @FXML
    private JFXButton btn_seance;
    @FXML
    private JFXButton btn_etudiant_add;
    @FXML
    private JFXButton btn_etudiant_delete;
    @FXML
    private JFXButton btn_etudiant_update;
    @FXML
    private JFXTextField txt_etud_nom;
    @FXML
    private JFXTextField txt_etud_prenom;
    @FXML
    private JFXTextField txt_etud_email;
    @FXML
    private JFXTextField txt_etud_tel;
    @FXML
    private TableView<Etudiant> tviewEtudiant;

    @FXML
    private TextField txt_etud_chercher_Nom;
    @FXML
    private TextField txt_etud_chercher_Prenom;
    @FXML
    private JFXButton btn_chrecher_etudiant;
    @FXML
    private Label auth_userName_label;
    @FXML
    private Label enc_email_label;
    @FXML
    private Label enc_password_label;
    @FXML
    private Label enc_tele_label;
    @FXML
    private Label enc_prenom_label;
    @FXML
    private Label enc_nom_label;
    @FXML
    private Label enc_nickname_label;
    //variables Panel Seance
    private Seance SeanceSelected;
    private Projet ProjetSelected;
    private Etudiant etudiantSelected;
    private Livrable LivrableSelected;
    @FXML
    private JFXComboBox<String> Combo_projet_Seance;
    @FXML
    private JFXComboBox<String> Combo_etudiant_Seance;
    @FXML
    private TableView<Seance> tviewSeance;
    @FXML
    private JFXTextField Text_Seance_Num;
    @FXML
    private JFXDatePicker Date_Seance;
    @FXML
    private JFXTextField Text_Seance_Heuredebut;
    @FXML
    private JFXTextField Text_Seance_Heurefin;
    @FXML
    private JFXRadioButton is_absent_Seance_oui;
    @FXML
    private JFXTextArea Text_commentaire_Seance;
    @FXML
    private JFXTextArea Text_todo_Seance;
    @FXML 
    private JFXButton btn_ajouter_Seance;
    @FXML 
    private JFXButton btn_modifier_Seance;
    @FXML 
    private JFXButton btn_supprimer_Seance;
    @FXML
    private JFXTextField Text_engagement;
     @FXML 
    private JFXButton btn_modifier_engagement;
    @FXML
    private TableView<Livrable> tviewlivrable;
    @FXML
    private JFXTextField Text_intitule_livrable;
    @FXML
    private JFXDatePicker Date_livrable;
    @FXML
    private JFXTextField Text_Pourcentage_livrable;
    @FXML 
    private JFXButton btn_ajouter_livrable;
    @FXML 
    private JFXButton btn_modifier_livrable;
    @FXML 
    private JFXButton btn_supprimer_livrable;
    @FXML
    private JFXButton btn_afich_note;
    //^^variables Panel Seance^^
    
   // Projet
    @FXML
    private TableView<Projet> tviewProjet;
    @FXML
    private TextField txtprojet_chercher;
    @FXML
    private JFXComboBox<Encadrant> cmb_prj_encadrant_extern;
    @FXML
    private JFXComboBox<Etudiant> cmb_prj_etudiant1;
    @FXML
    private JFXComboBox<Etudiant> cmb_prj_etudiant2;
    @FXML
    private JFXTextField txt_projet_intitule;
    @FXML
    private JFXTextField txt_dateDebut;
    @FXML
    private JFXTextField txt_dateFin;
      @FXML
    private JFXButton btn_add_proj;
    @FXML
    private JFXButton btn_update_proj;
    @FXML
    private JFXButton btn_delete_proj; 
    @FXML
    private JFXTextField encadrant_nick_tb;
    @FXML
    private JFXTextField encadrant_email_tb;
    @FXML
    private JFXTextField encadrant_pass_tb;
    @FXML
    private JFXTextField encadrant_tele_tb;
    @FXML
    private JFXTextField encadrant_prenom_tb;
    @FXML
    private JFXTextField encadrant_nom_tb;
    @FXML
    private JFXButton btn_edit_encadrant;
    @FXML
    private JFXButton btn_encadrant_delete;
    @FXML
    private Pane Alert_Pane;
    @FXML
    private Label lbletudiant;
    @FXML
    private Label lblPrj;
    @FXML
    private JFXButton btn_Info_prj;
    @FXML
    private Label lblAbsence;
    @FXML
    private Label lblPrj2;
    @FXML
    private Label lbletudiant2;
    @FXML
    private Label lblAbsence2;
    @FXML
    private Label lbltextsience;
    @FXML
    private Label lblTextEt;
    @FXML
    private Label lbltextabsence;
    
    // UI Panel swap code~~~~~~~~~~~Start~~~~~~~~~~~~~~~~~~~~~~~~
   
    @FXML
     private void handleButtonclick(javafx.scene.input.MouseEvent event) {
        if (event.getSource() == btn_encadrant) {
            
            pnl_encadrant.toFront();
        } else if (event.getSource() == btn_projet) {
         
        Projets_loadAll();
            pnl_projet.toFront();
        } else if (event.getSource() == btn_etudiant) {
            pnl_etudiant.toFront();
        } else if (event.getSource() == btn_seance) {
             RemplirComboboxProjet();
            pnl_seance.toFront();
        }
    }
     // UI Panel swap code~~~~~~~~~~~Finish~~~~~~~~~~~~~~~~~~~~~~~~
     // access encadrant data~~~~~~~~~~~Start~~~~~~~~~~~~~~~~~~~~~~~~
     public Encadrant initEncadrantData(Encadrant encadrant){
        authentified_encadrant = encadrant;
        auth_userName_label.setText(authentified_encadrant.getLogin());
        
        enc_email_label.setText(authentified_encadrant.getEmail());
        encadrant_email_tb.setText(authentified_encadrant.getEmail());
        
        enc_password_label.setText(authentified_encadrant.getPassword());
        encadrant_pass_tb.setText(authentified_encadrant.getPassword());
        
        enc_tele_label.setText(authentified_encadrant.getTel());
        encadrant_tele_tb.setText(authentified_encadrant.getTel());
        
        enc_prenom_label.setText(authentified_encadrant.getPrenom());
        encadrant_prenom_tb.setText(authentified_encadrant.getPrenom());
        
        enc_nom_label.setText(authentified_encadrant.getNom());
        encadrant_nom_tb.setText(authentified_encadrant.getNom());
        
        enc_nickname_label.setText(authentified_encadrant.getLogin());
        encadrant_nick_tb.setText(authentified_encadrant.getLogin());
        return authentified_encadrant;
    }
     // access encadrant data~~~~~~~~~~~finish~~~~~~~~~~~~~~~~~~~~~~~~
     // edit encadrant code~~~~~~~~~~~Start~~~~~~~~~~~~~~~~~~~~~~~~
    @FXML
    private void edit_encadrant_click(javafx.scene.input.MouseEvent event) throws Exception{
         String nick = authentified_encadrant.getLogin();
        if (event.getSource() == btn_edit_encadrant){
            if(encadrant_nick_tb.getText()!="" || encadrant_email_tb.getText()!="" || encadrant_pass_tb.getText()!=""
                    || encadrant_nom_tb.getText()!="" || encadrant_prenom_tb.getText()!="" || encadrant_tele_tb.getText()!=""){
                 ArrayList<Encadrant> liste = Encadrant_CRUD.getByNickname(nick);
                 for (Encadrant encadrant : liste) {
                      int id_encadrant = encadrant.getId();
                      encadrant.setLogin(encadrant_nick_tb.getText());
                      encadrant.setNom(encadrant_nom_tb.getText());
                      encadrant.setPrenom(encadrant_prenom_tb.getText());
                      encadrant.setEmail(encadrant_email_tb.getText());
                      encadrant.setPassword(encadrant_pass_tb.getText());
                      encadrant.setTel(encadrant_tele_tb.getText());
                      authentified_encadrant = encadrant;
                      
                       Encadrant_CRUD.modifier_encadrant(authentified_encadrant);
                       // update encadrant info label
                        auth_userName_label.setText(authentified_encadrant.getLogin());
                        enc_email_label.setText(authentified_encadrant.getEmail());
                        enc_password_label.setText(authentified_encadrant.getPassword());
                        enc_tele_label.setText(authentified_encadrant.getTel());
                        enc_prenom_label.setText(authentified_encadrant.getPrenom());
                        enc_nom_label.setText(authentified_encadrant.getNom());
                        enc_nickname_label.setText(authentified_encadrant.getLogin());
                 }
            }
        }
    }
     // edit encadrant code~~~~~~~~~~~finish~~~~~~~~~~~~~~~~~~~~~~~~
    //----------------------------------------------------------------------------------------------
    
    // navigation SignOut Code~~~~~~~~~~~Start~~~~~~~~~~~~~~~~~~~~~~~~
    @FXML
    public void signOut_button_clicked(ActionEvent event) throws IOException{
        Parent signOutParent = FXMLLoader.load(getClass().getResource("Login.fxml"));
        Scene signOutScene = new Scene(signOutParent);
        
        // gets stage info code
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(signOutScene);
        window.show();
    }
    // navigation toLoginPage code~~~~~~~~~~~finish~~~~~~~~~~~~~~~~~~~~~~~~
    
    // delete encadrant code~~~~~~~~~~~Start~~~~~~~~~~~~~~~~~~~~~~~~
    @FXML
    private void delete_encadrant_click(javafx.scene.input.MouseEvent event) throws Exception{
        String nick = authentified_encadrant.getLogin();
        if(event.getSource() == btn_encadrant_delete){
            ArrayList<Encadrant> liste = Encadrant_CRUD.getByNickname(nick);
                 for (Encadrant encadrant : liste) {
                     authentified_encadrant = encadrant;
                     Encadrant_CRUD.delete_encadrant(authentified_encadrant);
                 }   
        }
        Parent signOutParent = FXMLLoader.load(getClass().getResource("Login.fxml"));
        Scene signOutScene = new Scene(signOutParent);
        
        // gets stage info code
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(signOutScene);
        window.show();
    }
    // delete encadrant code~~~~~~~~~~~finish~~~~~~~~~~~~~~~~~~~~~~~~
    
    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setTableViewesColumns();
        etudiants_loadAll();
        
        encadrants_loadAll();
           combobox_settiing_etudiant(cmb_prj_etudiant1);
        combobox_settiing_etudiant(cmb_prj_etudiant2);
        combobox_settiing_encadrant(cmb_prj_encadrant_extern);
        //-----
        RemplirColumnSeanceAndLivrable();
       
       
    }
   
     // ---------------------Projet-------------
    ArrayList<SPE>spe;
    ArrayList<String> nbabsent;
    @FXML
    private void cmb_selected_event(ActionEvent event) {
        try {
            
            if(event.getSource()== cmb_prj_etudiant1)
        { System.out.println("Selected employee: " + spe.get(0).getCount()+" "+Projet_CRUD.Get_Info_Prj(authentified_encadrant.getLogin(), 2).get(0).getNom());
                System.out.println("Salary: " + cmb_prj_etudiant1.getSelectionModel().getSelectedItem().getId());
                
        }
        
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
@FXML
    private void prj_info_Click(javafx.scene.input.MouseEvent event) {
        lblTextEt.setText("Etudiant 2");
          lbltextabsence.setText("NB Seance");
          lbltextsience.setText("NB Absence");
        if(selected_projet !=null && authentified_encadrant != null)
        {try{
         Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        
        
        
       if(spe.size()>0 && spe.size()<2){
          lbletudiant.setText(spe.get(0).getNom()+ " "+spe.get(0).getPrenom());
          lblAbsence.setText(""+spe.get(0).getCount());
          lblPrj.setText(nbabsent.get(0));
          lbletudiant2.setText("");
          lblAbsence2.setText("");
          lblPrj2.setText("");
          lblTextEt.setText("");
          lbltextabsence.setText("");
          lbltextsience.setText("");
          
       }
       else if(spe.size()>1)
       {
       lbletudiant.setText(spe.get(0).getNom()+ " "+spe.get(0).getPrenom());
          lblAbsence.setText(""+spe.get(0).getCount());
          lblPrj.setText(nbabsent.get(0));
          lbletudiant2.setText(spe.get(1).getNom()+ " "+spe.get(1).getPrenom());
          lblAbsence2.setText(""+spe.get(1).getCount());
          lblPrj2.setText(nbabsent.get(1));
       }

       
            
       
        alert.getDialogPane().setContent(Alert_Pane);
        alert.showAndWait();
        }catch(Exception e)
        {
         e.printStackTrace();
        }}
    }

     @FXML
    private void GS_projet_Click(javafx.scene.input.MouseEvent event) throws Exception {
     if (event.getSource() == btn_add_proj) {
         //&& cbx_projet_encadrantIntern.getValue().toString() != "" && cbx_projet_encadrant.getValue().toString() != ""
         if(txt_projet_intitule.getText()!="" && txt_dateDebut.getText() !="" && txt_dateFin.getText() !="" )
         {  
             selected_etudiant=null;
             Encadrant encadrant=cmb_prj_encadrant_extern.getSelectionModel().getSelectedItem();
             selected_projet=new Projet(0,txt_projet_intitule.getText(), txt_dateDebut.getText() , txt_dateFin.getText(), authentified_encadrant ,encadrant);
             Projet_CRUD.insert(selected_projet);
            Etudiant_Projet_CRUD.insert(new Etudiant_Projet(0,cmb_prj_etudiant1.getValue(),selected_projet));
            Etudiant_Projet_CRUD.insert(new Etudiant_Projet(0,cmb_prj_etudiant2.getValue(),selected_projet));
            encadrant=null;
            selected_projet=null;
         }
           txt_projet_intitule.setText("");
           txt_dateDebut.setText("");
           txt_dateFin.setText("");
           
           cmb_prj_encadrant_extern.setAccessibleText("");
           cmb_prj_etudiant1.setAccessibleText("");
           cmb_prj_etudiant2.setAccessibleText("");
           
           Projets_loadAll();
        } else if (event.getSource() == btn_delete_proj) {
           if( selected_projet!=null )
           { Projet_CRUD.Delete(selected_projet);
             
           txt_projet_intitule.setText("");
           txt_dateDebut.setText("");
           txt_dateFin.setText("");       
           selected_projet=null;
           selected_etudiant_projet=null;
           Projets_loadAll();
           }
          
        } else if (event.getSource() == btn_update_proj) {
            if( selected_projet!=null )
           { Projet_CRUD.Update(new Projet(selected_projet.getId(), txt_projet_intitule.getText(), txt_dateDebut.getText(), txt_dateFin.getText()));
          
            txt_etud_email.setText("");
           selected_etudiant=null;
           txt_projet_intitule.setText("");
           txt_dateDebut.setText("");
           txt_dateFin.setText("");
           
           
           Projets_loadAll();
           }
        }   
    }
        @FXML
    private void Selected_index_Projet(javafx.scene.input.MouseEvent event) {
        selected_projet= (Projet)tviewProjet.getSelectionModel().getSelectedItem();
        txt_projet_intitule.setText(selected_projet.getIntitule());
        txt_dateDebut.setText(selected_projet.getDate_debut());
        txt_dateFin.setText(selected_projet.getDate_fin());
        
        try {
            spe=Projet_CRUD.Get_Info_Prj(authentified_encadrant.getLogin(), selected_projet.getId());
            nbabsent=Projet_CRUD.Get_nb_Seance_prj(authentified_encadrant.getLogin(), selected_projet.getId());
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
   // ----------------------------------- endProjet -----------------------
    
    @FXML
    private void GS_etudiant_Click(javafx.scene.input.MouseEvent event) {
        
     if (event.getSource() == btn_etudiant_add) {
         if(!"".equals(txt_etud_email.getText()) && !"".equals(txt_etud_nom.getText()) && !"".equals(txt_etud_prenom.getText())&& !"".equals(txt_etud_tel.getText()))
         {  Etudaint_CRUD.Ajouter(new Etudiant(0,txt_etud_nom.getText(), txt_etud_prenom.getText(), txt_etud_email.getText(), txt_etud_tel.getText()));}
           txt_etud_email.setText("");
           txt_etud_nom.setText("");
           txt_etud_prenom.setText("");
           txt_etud_tel.setText("");
          
          etudiants_loadAll();
        } else if (event.getSource() == btn_etudiant_delete) {
           if( selected_etudiant!=null )
           { Etudaint_CRUD.Remove(new Etudiant(selected_etudiant.getId(), txt_etud_nom.getText(), txt_etud_prenom.getText(), txt_etud_email.getText(), txt_etud_tel.getText()));
            txt_etud_email.setText("");
           txt_etud_nom.setText("");
           txt_etud_prenom.setText("");
           txt_etud_tel.setText("");
           selected_etudiant=null;
           etudiants_loadAll();
           }
          
        } else if (event.getSource() == btn_etudiant_update) {
            if( selected_etudiant!=null )
           { Etudaint_CRUD.Update(new Etudiant(selected_etudiant.getId(), txt_etud_nom.getText(), txt_etud_prenom.getText(), txt_etud_email.getText(), txt_etud_tel.getText()));
            txt_etud_email.setText("");
           txt_etud_nom.setText("");
           txt_etud_prenom.setText("");
           txt_etud_tel.setText("");
           selected_etudiant=null;
           etudiants_loadAll();
           }
        }   
    }
     private void etudiants_loadAll() {
        try {
          cmb_prj_etudiant1.getItems().clear();
            TableView_etudiant tvUtil = new TableView_etudiant();
            tvUtil.populate(tviewEtudiant, Etudaint_CRUD.Get_by_encadrant());
            ObservableList<Etudiant> liste = FXCollections.<Etudiant>observableArrayList();
            liste.addAll(Etudaint_CRUD.Get_by_encadrant());
            cmb_prj_etudiant1.setItems(liste);
            cmb_prj_etudiant2.setItems(liste);
            
            
            tvUtil = null;
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }
     private void encadrants_loadAll() {
        try {
            cmb_prj_encadrant_extern.getItems().clear();
            ObservableList<Encadrant> liste = FXCollections.<Encadrant>observableArrayList();
            liste.addAll(Encadrant_CRUD.Get_cmb_encadrant());
            cmb_prj_encadrant_extern.setItems(liste);

            
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }
     private void Projets_loadAll() {
        try {
          
            TableView_Projet tvUtil = new TableView_Projet();
            tvUtil.populate(tviewProjet, Projet_CRUD.GetAll(authentified_encadrant.getLogin()));
            
            tvUtil = null;
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }
    private void setTableViewesColumns() {
        try {
            // CURRENT_ACTION = "CHARGEMENT DES DOSSIERS";
            ArrayList<Etudiant> al1 = new ArrayList(); 
            ArrayList<Projet> al2 = new ArrayList();
            
            TableView_etudiant tvUtil1 = new TableView_etudiant();
            TableView_Projet tvUtil3 = new TableView_Projet();

            tvUtil1.populate(tviewEtudiant, al1);
            tvUtil3.populate(tviewProjet, al2);
            al1 = null;
            al2 = null;
            tvUtil1 = null;
            tvUtil3 = null;
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    @FXML
    private void Selected_index(javafx.scene.input.MouseEvent event) {
        selected_etudiant= (Etudiant)tviewEtudiant.getSelectionModel().getSelectedItem();
        
        txt_etud_nom.setText(selected_etudiant.getNom());
        txt_etud_prenom.setText(selected_etudiant.getPrenom());
        txt_etud_tel.setText(selected_etudiant.getTel());
        txt_etud_email.setText(selected_etudiant.getEmail());
    }

    @FXML
    private void Etud_text_changerd_Nom(InputMethodEvent event) {
        if(!"".equals(txt_etud_chercher_Nom.getText())){
        try {
            tviewEtudiant.getItems().clear();
          
            TableView_etudiant tvUtil = new TableView_etudiant();
            tvUtil.populate(tviewEtudiant, Etudaint_CRUD.Get_by_Nom(txt_etud_chercher_Nom.getText()));
            
            tvUtil = null;
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
        }
    }

    @FXML
    private void Etud_text_changerd_Prenom(InputMethodEvent event) {
        if(txt_etud_chercher_Prenom.getText() != ""){
        try {
            // CURRENT_ACTION = "CHARGEMENT DES DOSSIERS";
          
            TableView_etudiant tvUtil = new TableView_etudiant();
            tvUtil.populate(tviewEtudiant, Etudaint_CRUD.Get_by_Prenom(txt_etud_chercher_Prenom.getText()));
            
            tvUtil = null;
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
        }
    }

    @FXML
    private void recherch_etudiant_clicked(javafx.scene.input.MouseEvent event) {
       try { 
           if(txt_etud_chercher_Prenom.getText() != "" && txt_etud_chercher_Nom.getText() == ""){
                 
            TableView_etudiant tvUtil = new TableView_etudiant();
            tvUtil.populate(tviewEtudiant, Etudaint_CRUD.Get_by_Prenom(txt_etud_chercher_Prenom.getText()));
            
            tvUtil = null;
        
            }else if(txt_etud_chercher_Prenom.getText() == "" && txt_etud_chercher_Nom.getText() != ""){
            TableView_etudiant tvUtil = new TableView_etudiant();
            tvUtil.populate(tviewEtudiant, Etudaint_CRUD.Get_by_Nom(txt_etud_chercher_Nom.getText()));
            
            tvUtil = null;
       
            }else if(txt_etud_chercher_Prenom.getText() != "" && txt_etud_chercher_Nom.getText() != ""){
       
            tviewEtudiant.getItems().clear();
          
            TableView_etudiant tvUtil = new TableView_etudiant();
            tvUtil.populate(tviewEtudiant, Etudaint_CRUD.Get_by_Nom_Prenom(txt_etud_chercher_Nom.getText(),txt_etud_chercher_Prenom.getText()));
            
            tvUtil = null;
            } 
            }catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
            }
        }

    @FXML
    private void projet_recherche_action(ActionEvent event) {
         if(txtprojet_chercher.getText()!="")
         {
          try {
          
            TableView_Projet tvUtil = new TableView_Projet();
            tvUtil.populate(tviewProjet, Projet_CRUD.Get_by_Nom(txtprojet_chercher.getText(),authentified_encadrant.getLogin()));
            
            tvUtil = null;
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
         }
    }
    private void combobox_settiing_etudiant(JFXComboBox<Etudiant> cmb)
    {
     cmb.setEditable(true);
        cmb.setConverter(new StringConverter<Etudiant>() {

            @Override
            public String toString(Etudiant object) {
                    if (object == null){ return null;}
                return object.toString();
            }

            @Override
            public Etudiant fromString(String string) {
                Etudiant employee = new Etudiant();
                for(Etudiant emp : cmb.getItems()){
                    if((emp.getNom()+" "+emp.getPrenom()).equals(string)){
                        employee = emp;
                        break;
                    }
                }
                return employee;
            }
        });
    }
    private void combobox_settiing_encadrant(JFXComboBox<Encadrant> cmb)
    {
     cmb.setEditable(true);
        cmb.setConverter(new StringConverter<Encadrant>() {

            @Override
            public String toString(Encadrant object) {
                    if (object == null){ return null;}
                return object.toString();
            }

            @Override
            public Encadrant fromString(String string) {
                Encadrant e = new Encadrant();
                for(Encadrant emp : cmb.getItems()){
                    if((emp.getNom()+" "+emp.getPrenom()).equals(string)){
                        e = emp;
                        break;
                    }
                }
                return e;
            }
        });
    }
    

//-----Debut--------Methode Pour Panel Seance------------------
    private void RemplirColumnSeanceAndLivrable() {
        try {
          TableView_Seance SeanceView = new TableView_Seance();
          TableView_Livrable LivrableView = new TableView_Livrable();
          SeanceView.AddColumnsSeance(tviewSeance);
          LivrableView.AddColumnsLivrable(tviewlivrable);
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      //-------------Gestion Seance------------------
    private void RemplirtableviewSeance() {
        try {
           if(ProjetSelected != null && etudiantSelected != null){
           Seance_CRUD DaoSeance = new Seance_CRUD();
           TableView_Seance SeanceView = new TableView_Seance();
           tviewSeance.getItems().addAll(SeanceView.getRowsSeance(DaoSeance.GetAll(ProjetSelected, etudiantSelected)));
            }
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
     private void RemplirComboboxProjet() {
        Combo_projet_Seance.getItems().clear();
        ArrayList<Projet> projets = new ArrayList<>();
        ObservableList<String> liste = FXCollections.<String>observableArrayList();
        try {
            Projet_CRUD DaoProjet = new Projet_CRUD();
            projets = DaoProjet.GetAll(authentified_encadrant.getLogin());
            for (Projet p : projets) {
                liste.add(p.getIntitule());  
            }
            Combo_projet_Seance.setItems(liste);
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    @FXML
    private void projetSelected_Seance(ActionEvent event){
        ObservableList<String> liste = FXCollections.<String>observableArrayList();
        ArrayList<Etudiant> ls = new ArrayList<>();
        String chaine;
        Projet p ;
      try{  
            Projet_CRUD DaoProjet = new Projet_CRUD();
            String intitule =Combo_projet_Seance.getSelectionModel().getSelectedItem();
            p  = DaoProjet.GetByIntitule(intitule);
            ls = DaoProjet.GetEtudiantsFromProjet(p);
            //remplir combobox Etudiant
            for (Etudiant e : ls) {
                chaine = e.getId()+"-"+e.getNom()+e.getPrenom();
                liste.add(chaine);  
            }
            Combo_etudiant_Seance.setItems(liste);
            ProjetSelected = p;
         }catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @FXML
    private void etudiantSelected_Seance(ActionEvent event){
        try{
        if(ProjetSelected != null){
       StringBuilder str = new StringBuilder();
       String chaine =Combo_etudiant_Seance.getSelectionModel().getSelectedItem();
       for (int i = 0; i < chaine.length(); i++) {
           if(chaine.substring(i, i+1)!= "-"){
              str.append(chaine.substring(i, i+1));
           }else
               break;
        }
       String ch = str.toString();
       etudiantSelected = Etudaint_CRUD.GetByID(Integer.parseInt(ch));
        }
        }catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @FXML
    private void SelectedViewSeanceIndex(javafx.scene.input.MouseEvent event) {
        SeanceSelected= (Seance)tviewSeance.getSelectionModel().getSelectedItem();
        
        Text_Seance_Num.setText(SeanceSelected.getNum_seance()+"");
        Text_Seance_Heuredebut.setText(SeanceSelected.getHeure_debut());
        Text_Seance_Heurefin.setText(SeanceSelected.getHeure_fin());
        Text_commentaire_Seance.setText(SeanceSelected.getCommentaire());
        Text_todo_Seance.setText(SeanceSelected.getTodo());
        is_absent_Seance_oui.setSelected(SeanceSelected.isIsAbscent());
        //convert String to LocalDate
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate Date = LocalDate.parse(SeanceSelected.getDate_seance(), formatter);
        Date_Seance.setValue(Date);
    }   
    @FXML   
    private void GS_Seance_Click(javafx.scene.input.MouseEvent event) {
        try {
        boolean absent= false;
        Seance Seance;   
        if (event.getSource() == btn_ajouter_Seance) {
            if(Text_Seance_Num.getText()!="" && Date_Seance.getValue()!= null && 
            Text_Seance_Heuredebut.getText() !=""&& Text_Seance_Heurefin.getText()!=""){
            if(is_absent_Seance_oui.isSelected()){ absent = true;}
                   Seance = new Seance(Integer.parseInt(Text_Seance_Num.getText()),
                                        Text_commentaire_Seance.getText(),
                                        Text_todo_Seance.getText(),absent,
                                        Date_Seance.getValue().toString(),
                                        Text_Seance_Heuredebut.getText(),
                                        Text_Seance_Heurefin.getText());
           Seance_CRUD.insert(Seance);
           Text_Seance_Num.setText("");
           Text_Seance_Heuredebut.setText("");
           Text_Seance_Heurefin.setText("");
           Text_commentaire_Seance.setText("");
           Text_todo_Seance.setText("");
           is_absent_Seance_oui.setSelected(false);
           RemplirtableviewSeance();
           }
        }else if (event.getSource() == btn_supprimer_Seance) {
           if(SeanceSelected !=null )
              Seance_CRUD.Delete(SeanceSelected.getId());
           Text_Seance_Num.setText("");
           Text_Seance_Heuredebut.setText("");
           Text_Seance_Heurefin.setText("");
           Text_commentaire_Seance.setText("");
           Text_todo_Seance.setText("");
           is_absent_Seance_oui.setSelected(false);
           SeanceSelected = null;
           RemplirtableviewSeance();
        }else if (event.getSource() == btn_modifier_Seance) {
            if(SeanceSelected!= null ){
                 if(is_absent_Seance_oui.isSelected()){ absent = true;}
             Seance = Seance_CRUD.GetByID(SeanceSelected.getId());
             Seance.setNum_seance(Integer.parseInt(Text_Seance_Num.getText()));
             Seance.setDate_seance(Date_Seance.getValue().toString());
             Seance.setHeure_debut(Text_Seance_Heuredebut.getText());
             Seance.setHeure_fin(Text_Seance_Heurefin.getText());
             Seance.setIsAbscent(absent);
             Seance.setCommentaire(Text_commentaire_Seance.getText());
             Seance.setTodo(Text_todo_Seance.getText());
             
           Seance_CRUD.Update(Seance);
           Text_Seance_Num.setText("");
           Text_Seance_Heuredebut.setText("");
           Text_Seance_Heurefin.setText("");
           Text_commentaire_Seance.setText("");
           Text_todo_Seance.setText("");
           is_absent_Seance_oui.setSelected(false);
           RemplirtableviewSeance();
           }
        }   
    } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    
    }
   
      //-------------Gestion Seance------------------
        private void RemplirtableviewLivrable() {
        try {
            Livrable_CRUD DaoLivrable = new Livrable_CRUD();
            TableView_Livrable LivrableView = new TableView_Livrable();
           if(SeanceSelected != null) {
           tviewlivrable.getItems().clear();
           tviewlivrable.getItems().addAll(LivrableView.getRowsFromArrayList(DaoLivrable.GetBySeance(SeanceSelected)));
           }else{
               tviewlivrable.getItems().clear();
           }
               
        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }  
        @FXML
        private void SelectedViewLivrableIndex(javafx.scene.input.MouseEvent event) {
        LivrableSelected = (Livrable)tviewlivrable.getSelectionModel().getSelectedItem();
        
        Text_intitule_livrable.setText(LivrableSelected.getIntitule());
        Text_Pourcentage_livrable.setText(LivrableSelected.getPourcentage()+"");
        //convert String to LocalDate
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate Date = LocalDate.parse(LivrableSelected.getDate(), formatter);
        Date_livrable.setValue(Date);
    }   
    @FXML   
    private void GS_Livrable_Click(javafx.scene.input.MouseEvent event) {
        try {
        Livrable livr;
        if (event.getSource() == btn_ajouter_Seance) {
            if(Text_intitule_livrable.getText()!="" && Date_livrable.getValue()!= null && 
            Text_Pourcentage_livrable.getText() !=""&& SeanceSelected!=null){
                   livr = new Livrable( Date_livrable.getValue().toString(),
                           Text_intitule_livrable.getText(),
                           Double.parseDouble(Text_Pourcentage_livrable.getText()), SeanceSelected);
           Livrable_CRUD.insert(livr);
           Text_Pourcentage_livrable.setText("");
           Text_intitule_livrable.setText("");
           Date_livrable.setValue(null);
           RemplirtableviewLivrable();
           }
        }else if (event.getSource() == btn_supprimer_Seance) {
           if(LivrableSelected !=null ){
                Livrable_CRUD.Delete(LivrableSelected.getId());
                Text_Pourcentage_livrable.setText("");
                Text_intitule_livrable.setText("");
                Date_livrable.setValue(null);
                RemplirtableviewLivrable();
                SeanceSelected = null;
           }
        }else if (event.getSource() == btn_modifier_Seance) {
            if(LivrableSelected!= null ){
             livr = Livrable_CRUD.GetByID(LivrableSelected.getId());
             livr.setPourcentage(Double.parseDouble(Text_Pourcentage_livrable.getText()));
             livr.setDate(Date_livrable.getValue().toString());
             livr.setIntitule(Text_intitule_livrable.getText());
             
           Livrable_CRUD.Update(livr);
           Text_Pourcentage_livrable.setText("");
           Text_intitule_livrable.setText("");
           Date_livrable.setValue(null);
           RemplirtableviewSeance();
           }
        }   
    } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    
    }
    //-------------Gestion Livrable------------------
     @FXML
    private void afiich_click_note(javafx.scene.input.MouseEvent event) {
        try{   
            if(SeanceSelected != null){
                infoEtudiant_CRUD Dao = new infoEtudiant_CRUD();
                int e = Dao.GetEngagement(ProjetSelected, etudiantSelected, SeanceSelected);
                if(e == -1){
                    Text_engagement.setText("");
                }else
                    Text_engagement.setText(e+"");
            }
        }catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
        @FXML
    private void Click_engagement(javafx.scene.input.MouseEvent event) {
             Info_etudiant et;  
            int note;
         try{ 
            infoEtudiant_CRUD Dao = new infoEtudiant_CRUD();
            et = Dao.GetinfoEtudiant(ProjetSelected,etudiantSelected,SeanceSelected);
            note = Integer.parseInt(Text_engagement.getText());
            et.setEngagement(note);
            Dao.Update(et);
         }catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//-----Debut--------Methode Pour Panel Seance------------------   

    

    

    
   
    
}
        
    
    
    

