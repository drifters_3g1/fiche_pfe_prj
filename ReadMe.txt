Commit Features : Entity Etudiant + Etudiant_Prj attributes and associations.
Dev : Sallaoui Hassan
Date : 15-03-2020

--------------------------

Commit Features : Entity Projet + Encadrant + Livrable attributes and associations.
Dev : Sohail
Date : 16-03-2020

--------------------------

Commit Features : Entity Etudiant attributes Update
Dev : Mehdi
Date : 16-03-2020

--------------------------

Commit Features : Entity Seance  + Etudiant_Prj attributes update
Dev : Amine
Date : 17-03-2020

--------------------------


Commit Features : -Entity Etudiant_prj association Update 
		  -Entity Projet attributes + Associations Update
		  -Entity Livrable attributes + Associations Update
		  -Entity Seance Attributes + Associations Update 
Dev : Sohail
Date : 19-03-2020

--------------------------

Commit Features : -Entity Info_etudiant attributes + association ADD 
		  -ADD Hibernate.cfg.xml 
		  -ADD Getter & Setter & Constructeur to all Project 
Dev : Hassan
Date : 19-03-2020 
--------------------------

Commit Features : 
		  -UPDATE Constructeur to all Project 
Dev : Amine
Date : 19-03-2020 

--------------------------

Commit Features : 
		  -Create Package Hibernate.config 
		  -Create Classe Classe Hibernate_Config 
	          -Create Package Hibernate.CRUD
		  -Create Classe Etudiant_CRUD
		  -Create Classe NewClass Dans Package Hibernate.entities
Dev : Sallaoui Hassan
Date : 20-03-2020 

--------------------------
Commit Features : -Update hibernate_config.java
                  -Create Classe Projet_CRUD
Dev : Mehdi
Date : 21-03-2020
-------------------------
Commit Features : 
		  -Added Jfoenix and Fontawesome Library
		  -Created Login/Signup Page design in fxml
	          
Dev : Sohail Erzini
Date : 30-03-2020 
-------------------------
Commit Features : 
		  -Replace onction to onMouseclicked
	          
Dev : Sallaoui Hassan
Date : 31-03-2020 

-----------------------------------
Commit Features : 
		  -Encadrant GUI (informations pane design)
	          
Dev : Sohail Erzini
Date : 16-04-2020
-----------------------------------
Commit Features : 
		  -etudiant GUI (informations pane design)
	          
Dev : Sallaoui Hassan
Date : 16-04-2020 
-----------------------------------
Commit Features : 
		  -Projets GUI (Projets + livrables du projet panes)
	          
Dev : Sohail Erzini
Date : 21-04-2020
-----------------------------------
Commit Features : 
		  -TableView_util (Projets + livrables + Encadrant + Etudiant)
	          
Dev : Sallaoui Hassan
Date : 24-04-2020
----------------------------------
Commit Features : 
		  -Projets GUI update (new ui )
		  -Etudiant GUI update (new ui)
	          
Dev : Sohail Erzini
Date : 28-04-2020
----------------------------------
Commit Features : 
		  -Seance GUI  (new ui )
		  -Encadrant GUI update
	          
Dev : Sohail Erzini
Date : 02-05-2020
----------------------------------
Commit Features : 
		  -Etudiant(Add,Update,Delete, Recherche)
Dev : Sallaoui Hassan
Date : 08-05-2020
-----------------------------------
Commit Features : 
		  -SignUp ( design + creation compte encadrant + retour au login)
		  -Login ( encadrant login form+ validation)
Dev : Sohail Erzini
Date : 09-05-2020
-----------------------------------
Commit Features : 
		  -PaneProjet(tableview(getall()) ,Recherche_by_intitule , Combobox Settings_encadrant,Combobox Settings_Etudiant)
		  -Encadrant_Crud(Get_cmb_encadrant)
		  -Encadrant(update)
Dev : Sallaoui Hassan
Date : 09-05-2020
-----------------------------------
-----------------------------------
Commit Features : 
		  update code login 
		  data load encadrant code
Dev : Sohail Erzini 
Date : 09-05-2020
-----------------------------------
Commit Features : -Add Function hibernate_config
                  -add unique Column intitule {Projet}
                  -Add Package Seance_CRUD
                  -Add Package Livrable_CRUD
                  -Add package infoEtudiant_CRUD
                  -Add Package TableView_Seance
                  -update Package TableView_Livrable
                  -Add Function Projet_CRUD
                  -Add Function Etudiant_CRUD
                  -Add CodeSource Panel Seance(gestion seance/livrable/evaluation)
Dev : Mehdi
Date : 09-05-2020
---------------------------------
Commit Features : 
		  - CodeSource Panel Projet(Add - delete - Update - Selected_Projet)
Dev : Amine
Date : 9-05-2020
-----------------------------------
Commit Features : 
		  -update CodeSource Panel Projet(Add - delete - Update)
Dev : Sallaoui Hassan
Date : 10-05-2020
-----------------------------------
-----------------------------------
Commit Features : 
		  Encadrant_CRUD ( selectbyname + update + delete )
		  Panel Encadrant (Code modification + suppresion encadrant )
		  Logout code
			
Dev : Sohail Erzini
Date : 10-05-2020
-----------------------------------
Commit Features : 
		  -Alert PRojet info
Dev : Sallaoui Hassan
Date : 10-05-2020
-----------------------------------